import java.util.*;

public class Main
{
	public static void main(String[] args) 
	{
	    Scanner in = new Scanner(System.in);
	    int n = in.nextInt();
	    
	  String[] array = new String[n];
	  
	 System.out.println("Enter strings : ");
     for(int i=0;i<n;i++)
      {
    	 Scanner scan = new Scanner(System.in);
          array[i] = scan.nextLine();
      }

       for(int i = 0; i<n-1; i++) 
       {
         for (int j = i+1; j<array.length; j++) 
         {
            if(array[i].compareTo(array[j])>0)
            {
               String temp = array[i];
               array[i] = array[j];
               array[j] = temp;
            }
         }
      }
      
      System.out.println("\nSorted Array : ");
      for(int i=0;i<n;i++)
      {
          System.out.println(array[i]);
      }
      
		 
	}
}
