import java.util.*;
public class practice2
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the year:");
		int year = sc.nextInt();
		/*
		boolean z=false;
		if((year%4==0 && year%100!=0)|| year%400==0)
		{
			System.out.println("Leap Year");
		}
		else
			System.out.println("Non-leap year");
		*/
		GregorianCalendar calendar = new GregorianCalendar();
		if(calendar.isLeapYear(year))
			System.out.println("Leap year");
		else
			System.out.println("Non-leap year");
	}
}
